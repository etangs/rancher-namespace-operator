FROM golang:1.20-alpine as compil-stage
WORKDIR /workspace
COPY . /workspace
RUN go mod tidy
RUN go build ./cmd/main/
FROM busybox:1.36
COPY --from=compil-stage /workspace/main /main
ENTRYPOINT /main
