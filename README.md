# rancher-namespace-operator

## How to install
```
helm repo add etangs https://gitlab.com/api/v4/projects/46383972/packages/helm/stable
helm repo update
helm search repo etangs
helm install myrelease etangs/rancher-namespace-operator
```