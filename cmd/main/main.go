package main

import (
	"gitlab.com/etangs/rancher-namespace-operator/internal/utils"
	"time"
	"strings"
	"strconv"
	"log"
)

func main() {
	// read parameters from env
	debug,_:=strconv.Atoi(utils.GetEnv("DEBUG","0"))
	log.Println("[INFO]: Debug = %d",debug)
	ref_namespace:=utils.GetEnv("REF_NAMESPACE","default")
	log.Println("[INFO]: Reference namespace for annotation = %s",ref_namespace)
	ref_annotation_key:=utils.GetEnv("REF_ANNOTATION","managed")
	log.Println("[INFO]: Reference key for annotation = %s",ref_annotation_key)
	excluded_namespaces:=strings.Split(utils.GetEnv("IGNORED_NAMESPACES","kube-system,local,dashboard"),",")
	log.Println("[INFO]: Unwatched namespaces are:")
	for _, ns := range excluded_namespaces {
        log.Println("   %s",ns)
    }

	nsm:=utils.NewNamespaceManager(debug)

	// get referance for annotation
	ref_annotation_value:=nsm.GetAnnotation(ref_namespace,ref_annotation_key)
	if ref_annotation_value == "" {
		log.Panicln("[FATAL]: Reference namespace does not have reference annotation")
	}

	i:=0
	for {
		// scan all namespaces
		for _,namespace:= range nsm.GetNamespaces() {
			if i==0 {
				log.Println("[INFO]: Watched namespace %s",namespace)
			}
			if (nsm.GetAnnotation(namespace,ref_annotation_key)=="") && !(utils.Contains(excluded_namespaces,namespace)) {
				log.Println("[INFO]: ... add annotation %s=%s to namespace %s",ref_annotation_key,ref_annotation_value,namespace)
				nsm.AddAnnotation(namespace,ref_annotation_key,ref_annotation_value)
			}
		}
		i=i+1
		time.Sleep(10 * time.Second)
	}
}
