package utils

import (
	"context"
	"fmt"
	"os"

	"flag"
	"path/filepath"

	"encoding/json"

	//"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	//"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/apimachinery/pkg/types"

	"k8s.io/client-go/util/homedir"
	"k8s.io/client-go/tools/clientcmd"
)

type patchStringValue struct {
    Op    string `json:"op"`
    Path  string `json:"path"`
    Value string `json:"value"`
}

type NamespaceManager struct {
	client *kubernetes.Clientset
	debug int
}

func NewNamespaceManager(debug int) *NamespaceManager {
	if debug ==1 {
		//external cluster
		var kubeconfig *string
		if flag.Lookup("kubeconfig") == nil {
			if home := homedir.HomeDir(); home != "" {
				kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
			} else {
				kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
			}
		}
		flag.Parse()
		// use the current context in kubeconfig
		config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
		if err != nil {
			panic(err.Error())
		}
		// creates the clientset
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			panic(err.Error())
		}
		return &NamespaceManager{debug: debug, client: clientset}
	} else {
		//internal cluster
		config, err := rest.InClusterConfig()
		if err != nil {
			panic(err.Error())
		}
		// creates the clientset
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			panic(err.Error())
		}
		return &NamespaceManager{debug: debug, client: clientset}
	}
}

func (nsm NamespaceManager) GetAnnotation(namespace string, key string) string {
	res:=""
	ns, _ :=  nsm.client.CoreV1().Namespaces().Get(context.TODO(), namespace, metav1.GetOptions{})
	annotations:=ns.GetAnnotations()
	for annotation_name, annotation_value := range annotations{
		if annotation_name == key {
			res= annotation_value
		}
	}
	return res
}

func (nsm NamespaceManager) AddAnnotation(namespace string, key string, value string){
	ns, _ :=  nsm.client.CoreV1().Namespaces().Get(context.TODO(), namespace, metav1.GetOptions{})
	annotations:=ns.GetAnnotations()
	if len(annotations)!=0 {
		annotations[key]=value
	} else {
		annotations=map[string]string{
			key: value,
		}
	}
	ns.SetAnnotations(annotations)
	_,updateErr :=nsm.client.CoreV1().Namespaces().Update(context.TODO(), ns, metav1.UpdateOptions{})
	if updateErr != nil {
		fmt.Println(updateErr)
	}
}

func (nsm NamespaceManager) AddLabel(namespace string, key string, value string){
	payload := []patchStringValue{{
		Op:    "add",
		Path:  fmt.Sprintf("/metadata/labels/%s",key),
		Value: value,
	}}
	payloadBytes, _ := json.Marshal(payload)
	_, updateErr := nsm.client.CoreV1().Namespaces().Patch(context.TODO(), namespace, types.JSONPatchType, payloadBytes, metav1.PatchOptions{})
	if updateErr != nil {
		fmt.Println(updateErr)
	}
}

// List all namespaces of K8S cluster
func (nsm NamespaceManager) GetNamespaces() []string {
	namespaces, err := nsm.client.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{})
	res:=make([]string,len(namespaces.Items))
	if err != nil {
		panic(err.Error())
	}
	for i, ns := range namespaces.Items {
		res[i]=ns.GetName()
	}
	return res
}

func GetEnv(key string, fallback string) string {
    value, exists := os.LookupEnv(key)
    if !exists {
        value = fallback
    }
    return value
}

func Contains(s []string, e string) bool {
    for _, a := range s {
        if a == e {
            return true
        }
    }
    return false
}